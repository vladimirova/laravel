@extends('app')

@section('content')
    <h1>Article</h1>
    <article>
        <h2>
            <a href="{{ action('ArticlesController@show', [$article->id]) }}"> {{ $article -> title }}</a>
        </h2>
        <div class="body">{{ $article->body }}</div>
    </article>

    @unless($article->tags->isEmpty())
        <h2>Tags:</h2>
        <ul>
            @foreach($article->tags as $tag)
                <li>{{ $tag->name }}</li>
            @endforeach
        </ul>
    @endunless
@stop