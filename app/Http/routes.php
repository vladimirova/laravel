<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');


Route::resource('articles', 'ArticlesController');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController'
]);

//Route::get('articles/create', 'ArticlesContoller@create');
//Route::get('articles/{id}', 'ArticlesContoller@show');
//Route::get('articles', 'ArticlesContoller@index');
//Route::post('articles', 'ArticlesContoller@store');
//Route::get('acticles/{id}/edit', 'ArticlesContoller@edit');
