<?php namespace App\Http\Controllers;

use Auth;
use App\Article;
use App\Tag;
use App\Http\Requests;
use App\Http\Requests\ArticleRequest;
use Illuminate\HttpResponse;
use Illuminate\Http\Request;
//use Illuminate\Routing\Controller;
use Carbon\Carbon;

class ArticlesController extends Controller {

    public function __construct()
    {
        $this->middleware('auth', ['except' => 'index']);
//        $this->middleware('auth', ['only' => 'index']);
    }

	public function index()
    {
        $articles = Article::latest('published_at')->published()->get();
        return view('articles.index', compact('articles'));
    }

    public function show($id)
    {
        $article = Article::findOrFail($id);
//        dd($article);
        return view('articles.show', compact('article'));
    }

    public function create()
    {
        $tags = Tag::lists('name', 'id');

        return view('articles.create', compact('tags'));
    }

    /**
     * @param ArticleRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ArticleRequest $request)
    {
        $this->createArticle($request);


//        $article = new Article($request->all());
//        Auth::user()->articles; //colection
//        Article::create($request->all());

//        session()->flash('flash_message', 'Your article has been created!');

//        flash()->success('Your article has been created!');
        flash()->overlay('Your article has been created!', 'good');

        return redirect('articles');

//        return redirect('articles')->with([
//            'flash_message' => 'Your article has been created!',
//            'flash_message_important' => true
//        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $article = Article::findOrFail($id);

        $tags = Tag::lists('name', 'id');

        return view('articles.edit', compact('article','tags'));
    }

    public function update($id, ArticleRequest $request)
    {
        $article = Article::findOrFail($id);

        $article->update($request->all());
        $article->tags()->sync($request->input('tag_list'));

        return redirect('articles');
    }

}
